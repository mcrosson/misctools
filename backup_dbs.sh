#!/usr/bin/env bash

backup_dir="/home/mcrosson/db_backup"

if [ ! -d $backup_dir ];
then
	mkdir $backup_dir
fi

host=`hostname`
timestp=`date '+%Y%m%d.%H%M'`

if [ $host != "influenza" ]; then
	for db in `psql -t -d template1 -c "select datname from pg_database where datname not like '%template%';"`;
	do
		trap `rm ${backup_dir}/*${db}*` ERR
		set +e
		pg_dump -bcxO -E utf-8 --role=postgres -f ${backup_dir}/${timestp}.${db}.sql ${db}
		set -e
	done
fi

if [ $host == "influenza" ]; then
	# MySQL DB backup
	passwd=`cat ~/opt/bin/mysql.pw`
	trap `rm /home/mcrosson/db_backup/*mysql*` ERR
	mysqldump -u root -p${passwd} --events --all-databases > ${backup_dir}/${timestp}.mysql_all_dbs.sql

	# Postgresql DB Backup
	for db in `sudo -u postgres psql -t -d template1 -c "select datname from pg_database where datname not like '%template%';"`;
	do
		trap `rm ${backup_dir}/*${db}*` ERR
		set +e
		pg_dump -bcxO -E utf-8 --role=postgres -f ${backup_dir}/${timestp}.${db}.sql ${db}
		set -e
	done
fi
