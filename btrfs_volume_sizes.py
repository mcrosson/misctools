#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import subprocess, pprint

cut_bin = '/usr/bin/cut'
btrfs_bin = '/sbin/btrfs'

# Sane formatting of human readable byte strings
def sizeof_fmt(num):
	for x in ['bytes','KB','MB','GB']:
		if abs(num) < 1024.0:
			return "%3.1f%s" % (num, x)
		num /= 1024.0
	
	return "%3.1f%s" % (num, 'TB')

def convertTB(size):
	size = size[0:-1]
	size = float(size) * 1024 * 1024 * 1024 * 1024
	return size

def convertGB(size):
	size = size[0:-1]
	size = float(size) * 1024 * 1024 * 1024
	return size

def convertMB(size):
	size = size[0:-1]
	size = float(size) * 1024 * 1024
	return size

def convertKB(size):
	size = size[0:-1]
	size = float(size) * 1024
	return size

def convertToBytes(number):
	number= number.strip()
	size = None
	if number[-1] == 'K':
		size = convertKB(number)
	elif number[-1] == 'M':
		size = convertMB(number)
	elif number[-1] == 'G':
		size = convertGB(number)
	elif number[-1] == 'T':
		size = convertTB(number)
	return size


# Print a tab w/o a new line @ end of line
def print_tab():
        print('    ', end='')


if __name__ == '__main__':
	btrfs = subprocess.Popen([btrfs_bin, 'subvolume', 'list', '/'], stdout=subprocess.PIPE)
	cut = subprocess.Popen([cut_bin, '-d', ' ', '-f', '7'], stdout=subprocess.PIPE, stdin=btrfs.stdout)
	volumes = []
	for line in cut.stdout:
		volumes.append(line.strip())
	
	pprint.pprint(volumes)

