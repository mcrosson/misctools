#!/usr/bin/env python
#-*- coding: utf-8 -*-

########################################
# Rebalance the used space on a zfs pool
########################################

import os, subprocess, random, shutil

all_files_filename = 'zrf_scratch_cache'
pool = 'tank'
zfs_bin = '/sbin/zfs'
zpool_bin = '/sbin/zpool'
grep_bin = '/bin/grep'
zvol = '/tank/rebalance/'
rsync = '/usr/bin/rsync'

# Snapshot the filesystem for safety's sake
#subprocess.check_call(['zfs', 'snapshot', '-r', '@'.join([pool, 'zrf'])])

# All files on the filesystem
all_files = [ ]

# Load the list of files on the file system
#     Generate if not pre-existing
if not os.path.isfile(all_files_filename):
	with open(all_files_filename, 'w') as w:
		# Get list of all files on the filesystem
		for dirpath, dirnames, filenames in os.walk('/scratch'):
			for file in filenames:
				full_file_path = os.path.join(dirpath, file)
				all_files.append(full_file_path)
				w.write(full_file_path)
				w.write('\n')
else:
	with open(all_files_filename, 'r') as r:
		for line in r:
			all_files.append(line.strip())

with open('zrf_scratch_processed', 'r') as pf:
	for line in pf:
		all_files.remove(line.strip())

processed_files = open('zrf_scratch_processed', 'a')

print len(all_files)

while len(all_files) > 0:
	index = random.randint(0, len(all_files) - 1)
	source = all_files.pop(index)
	filename = os.path.split(source)[-1]
	destination = os.path.join(zvol, filename)
	try:
		statinfo = os.stat(source)
		print(str(round(statinfo.st_size / 1024.0 / 1024.0, 2)) + 'M: ' + filename)
		#stats = os.stat(source)
		#uid = stats.st_uid
		#gid = stats.st_gid
		#shutil.copy2(source, destination)
		#os.chown(destination, uid, gid)
		subprocess.check_call([rsync, '-a', '-P', '-r', source, destination])
		os.remove(source)
		#shutil.copy2(destination, source)
		#os.chown(source, uid, gid)
		subprocess.check_call([rsync, '-a', '-P', '-r', destination, source])
		os.remove(destination)
		processed_files.write(source)
		processed_files.write('\n')
	except OSError:
		print('    Failed to copy')
	except IOError:
		print('    Failed to copy')

print('Don\'t forget to delete zrf_cache if you are not going to run this for awhile')
print('This program created a snapshot before running, delete if it ran right')
