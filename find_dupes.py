#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os, pprint
from collections import defaultdict

possible_dupes = defaultdict(list)

for dirpath, dirnames, filenames in os.walk('/tank/Video/'):
	# Ignore files processed for portable devices
	if dirpath.startswith('/tank/Video/Portable') or dirpath.startswith('/tank/Video/Movies/Critical'):
		continue
	for file in filenames:
		# Ignore osX .ds_store files
		if file.endswith('.DS_STORE') or file.endswith('.DS_Store'):
			continue
		# Ignore XP thumbnail files
		if file.endswith('Thumbs.db'):
			continue
		# Ignore vista / 7 thumbnail files
		if file.endswith('ehthumbs_vista.db'):
			continue
		# Ignore to_verify folders
		if 'to_verify' in file or 'to_verify' in dirpath:
			continue
		possible_dupes[file].append(os.path.join(dirpath, file))

for filename in possible_dupes:
	if len(possible_dupes[filename]) > 1:
		pprint.pprint(possible_dupes[filename])

