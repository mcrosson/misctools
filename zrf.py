#!/usr/bin/env python
#-*- coding: utf-8 -*-

########################################
# Rebalance the used space on a zfs pool
########################################

import os, subprocess, random, shutil, time, sys
from multiprocessing import Pool

all_files_filename = 'zrf_cache'
pool = 'tank'
zfs_bin = '/sbin/zfs'
zpool_bin = '/sbin/zpool'
grep_bin = '/bin/grep'
zvol = '/tank/rebalance/'
rsync = '/usr/bin/rsync'

# All files on the filesystem
all_files = [ ]

# Load the list of files on the file system
#     Generate if not pre-existing
if not os.path.isfile(all_files_filename):
	with open(all_files_filename, 'w') as w:
		# Get list of all files on the filesystem
		for dirpath, dirnames, filenames in os.walk(os.path.join('/', pool)):
			for file in filenames:
				full_file_path = os.path.join(dirpath, file)
				all_files.append(full_file_path)
				w.write(full_file_path)
				w.write('\n')
else:
	with open(all_files_filename, 'r') as r:
		for line in r:
			all_files.append(line.strip())

# remove processed files from the list of all files
#    method for multi-processing
def remove_files(line):
	#print('processing line: ' + line.strip())
	try:
		all_files.remove(line.strip())
	except ValueError:
		print('Invalid file in processed list: ' + line.strip())
	

#process_pool = Pool(processes=3)

# remove files from the list of all files
with open('zrf_processed', 'r') as pf:
	#process_pool.map(remove_files, pf, 1024)
	for line in pf:
		remove_files(line)
	
# make sure the processed file exists before opening it for append
if not os.path.isfile('zrf_processed'):
	with open('zrf_processed') as tmp:
		tmp.write('\n')

processed_files = open('zrf_processed', 'a')

def convertTB(size):
	size = size[0:-1]
	size = float(size) * 1024 * 1024 * 1024
	return size

def convertGB(size):
	size = size[0:-1]
	size = float(size) * 1024 * 1024
	return size

def convertMB(size):
	size = size[0:-1]
	size = float(size) * 1024
	return size

# Function to find used space on all of the pools mirrors
def find_utilization():
	zpool = subprocess.Popen([zpool_bin, 'iostat', '-v', pool], stdout=subprocess.PIPE)
	grep = subprocess.Popen([grep_bin, 'mirror'], stdin=zpool.stdout, stdout=subprocess.PIPE)
	usage = [ ]
	for line in grep.stdout:
		fields = line.strip().split()
		used = fields[1]
		#free = fields[2]
		if used[-1] == 'M':
			used = convertMB(used)
		elif used[-1] == 'G':
			used = convertGB(used)
		elif used[-1] == 'T':
			used = convertTB(used)
		usage.append(used)
	
	percent_usage = [ ]

	max_used = max(usage)
	for item in usage:
		percent_usage.append(item / max_used)
	
	return percent_usage

def free_space_distributed():
	utilization = find_utilization()
	for item in utilization:
		if item < .9:
			return False
	
	return True

# Snapshot the filesystem for safety's sake
#subprocess.check_call(['zfs', 'snapshot', '-r', '@'.join([pool, 'zrf'])])

while len(all_files) > 0:
	index = random.randint(0, len(all_files))
	source = all_files.pop(index)
	filename = os.path.split(source)[-1]
	destination = os.path.join(zvol, filename)
	try:
		statinfo = os.stat(source)
		print(str(round(statinfo.st_size / 1024.0 / 1024.0, 2)) + 'M: ' + source)
		#stats = os.stat(source)
		#uid = stats.st_uid
		#gid = stats.st_gid
		subprocess.check_call([rsync, '-a', '-P', '-r', source, destination])
		#shutil.copy2(source, destination)
		#os.chown(destination, uid, gid)
		os.remove(source)
		subprocess.check_call([rsync, '-a', '-P', '-r', destination, source])
		#shutil.copy2(destination, source)
		#os.chown(source, uid, gid)
		os.remove(destination)
		processed_files.write(source)
		processed_files.write('\n')
	except OSError:
		print('    Failed to copy')
	except IOError:
		print('    Failed to copy')

print('Don\'t forget to delete zrf_cache if you are not going to run this for awhile')
print('This program created a snapshot before running, delete if it ran right')
