#!/usr/bin/env bash
#Tools:
#	dvdbackup
#	ffmpeg
#	lsdvd
#	subtitleripper
#	ogmtools
#	mkvtoolnix

# Ignore spaces for the run
SAFEIFS=$IFS
IFS=$(echo -en "\n\b")

echo "Enter DVD rom"
read DVDROM
lsdvd $DVDROM | head -n 1
echo "Enter DVD's Name:"
read DVDNAME
WORKINGDIR="/Temp/${DVDNAME}"
mkdir $WORKINGDIR
cd $WORKINGDIR
dvdbackup -F -i $DVDROM -o ./
find ./ -iname *.vob | xargs -I '{}' mv '{}' ./
find ./ -type d | xargs -I '{}' rm -r '{}'
echo "Which chapter does the following correlate to?"
echo `ls | head -n 1`
read TITLE
dvdxchap -t $TITLE $DVDROM > dvd.chapters
eject $DVDROM
ls *.VOB | head -n 1 | xargs rm
cat *.VOB > movie.vob
rm VTS_*
echo "Press enter to run ffmpeg and mkvmerge"
read JUNKVAR
# 480p pass 1
 ffmpeg -i movie.vob -an -y -pass 1 -passlogfile pass.log \
  -mbd rd -flags 4mv+trell+aic+qprd+mv0+loop -cmp 2 -subcmp 2 -refs 6 -bf 3 \
  -partitions +parti4x4+parti8x8+partp4x4+partp8x8+partb8x8 \
  -flags2 +bpyramid+wpred+mixed_refs+4x4dct+8x8dct+brdo \
  -r 23.976023976 -b 3750k -bt 3750k -maxrate 7500k \
  -vcodec h264 -s 720x480 -level 51 -aspect 16:9 \
  -threads auto movie.h264 
# 480p pass 2
 ffmpeg -i movie.vob -an -y -pass 2 -passlogfile pass.log \
  -mbd rd -flags 4mv+trell+aic+qprd+mv0+loop -cmp 2 -subcmp 2 -refs 6 -bf 3 \
  -partitions +parti4x4+parti8x8+partp4x4+partp8x8+partb8x8 \
  -flags2 +bpyramid+wpred+mixed_refs+4x4dct+8x8dct+brdo \
  -r 23.976023976 -b 3750k -bt 3750k -maxrate 7500k \
  -vcodec h264 -s 720x480 -level 51 -aspect 16:9 \
  -threads auto movie.h264 
ffmpeg -i movie.vob -vn -acodec copy movie.ac3
mkvmerge -o /Video/Movies/${DVDNAME}.mkv  --aspect-ratio 0:16/9 --default-duration 0:24000/1001fps -d 0 -A -S movie.h264 -a 0 -D -S movie.ac3 --track-order 0:0,1:0 --title $DVDNAME --chapters dvd.chapters
echo $DVDNAME
# Turn spaces back on
IFS=$SAFEIFS
