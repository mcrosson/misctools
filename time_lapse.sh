#!/usr/bin/env bash
guvcview --no_display --format yuyv --size=1280x720 --cap_time=$1 --npics=$2
