import os, subprocess, shutil, glob

# ffmpeg pass1
ffmpegPass1 = [
	'ffmpeg', '-i', 'movie.vob', '-an', '-y', '-pass', '1', '-passlogfile', 'pass.log',
		'-mbd', 'rd', '-flags', '4mv+trell+aic+qprd+mv0+loop', '-cmp', '2', '-subcmp', '2', '-refs', '6', '-bf', '3',
		'-partitions', '+parti4x4+parti8x8+partp4x4+partp8x8+partb8x8',
		'-flags2', '+bpyramid+wpred+mixed_refs+4x4dct+8x8dct+brdo',
		'-r', '23.976023976', '-b', '3750k', '-bt', '3750k', '-maxrate', '7500k',
		'-vcodec', 'h264', '-s', '720x480', '-level', '41', '-aspect', '16:9',
		'-threads', 'auto', 'movie.h264'
]

# ffmpeg pass2
ffmpegPass2 = [
	'ffmpeg', '-i', 'movie.vob', '-an', '-y', '-pass', '2', '-passlogfile', 'pass.log',
		'-mbd', 'rd', '-flags', '4mv+trell+aic+qprd+mv0+loop', '-cmp', '2', '-subcmp', '2', '-refs', '6', '-bf', '3',
		'-partitions', '+parti4x4+parti8x8+partp4x4+partp8x8+partb8x8',
		'-flags2', '+bpyramid+wpred+mixed_refs+4x4dct+8x8dct+brdo',
		'-r', '23.976023976', '-b', '3750k', '-bt', '3750k', '-maxrate', '7500k',
		'-vcodec', 'h264', '-s', '720x480', '-level', '41', '-aspect', '16:9',
		'-threads', 'auto', 'movie.h264 '
]

# ffmpeg audio copy
ffmpegAudioCopy = ['ffmpeg', '-i', 'movie.vob', '-vn', '-acodec', 'copy', 'movie.ac3']

# Working directory for storing DVDs
workingDir = '/Temp'

# Root directory containing all TV shows
tvShowsStore = '/Video/TV Shows'

# Get information necessary for processing the TV Show
dvdrom = raw_input('DVD Drive: ')
tvshow = raw_input('Show Name: ')
season = raw_input('Season: ')
startEpisode = raw_input('Start Episode: ')

# Would dump lsdvd output to less and interact here but couldn't figure it out
titles = raw_input('Titles to rip (csv format): ')

# Index so episode numbers come out correctly
i = 0
# Process each episode stored on the dvd
for title in titles.split(','):
	# Tell the user which episode is being processed
	episode = '%s - S%2.2d E%2.2d' % (tvshow, int(season), int(startEpisode) + i)
	mkv = episode + '.mkv'
	print 'Processing: %s' % episode
	# Create the individual episodes working directory
	try:
		os.mkdir(os.path.join(workingDir, episode))
	except Exception:
		pass
	# Change the CWD to be the dir for the given episode
	os.chdir(os.path.join(workingDir, episode))
	# Backup the episode
	dvdbackup = subprocess.Popen(['dvdbackup', '-i', dvdrom, '-o', './', '-t', title])
	dvdbackup.communicate()
	# Write chapter information about the episode
	dvdChapters = open('dvd.chapters', 'w')
	dvdxchap = subprocess.Popen(['dvdxchap', '-t', title, dvdrom], stdout=dvdChapters)
	dvdxchap.communicate()
	dvdChapters.close()
	# Move movie's vobs up to the root of the working directory
	for root, dirs, files in os.walk('./'):
		for file in files:
			if file.endswith('.VOB'):
				shutil.move(os.path.join(root, file), os.path.join(workingDir, episode, file))
	# Concatenate vobs into one large vob for processing
	movie = open('movie.vob', 'wb')
	for vob in sorted(glob.glob('*.VOB')):
		vobFD = open(vob)
		movie.write(vobFD.read())
		os.remove(vob)
	
	# Process Video
	pass1Process = subprocess.Popen(ffmpegPass1)
	pass1Process.communicate()
	pass2Process = subprocess.Popen(ffmpegPass2)
	pass2Process.communicate()
	
	# Process Audio
	audioProcess = subprocess.Popen(ffmpegAudioCopy)
	audioProcess.communicate()
	
	# Create destination directory
	seasonDir = 'Season %2.2d' % int(season)
	outputDir = os.path.join(tvShowsStore, tvshow, seasonDir)
	try:
		os.mkdirs(outputDir)
	except Exception:
		pass
	
	# Merge converted files into one mkv
	mkvmerge = [
		'mkvmerge', '-o', os.path.join(outputDir, episode + '.mkv'),
			'--aspect-ratio', '0:16/9', '--default-duration', '0:24000/1001fps',
			'-d', '0', '-A', '-S', 'movie.h264',
			'-a', '0', '-D', '-S', 'movie.ac3',
			'--track-order', '0:0,1:0', '--title', episode, '--chapters', 'dvd.chapters'
	]
	mkvmergeProcess = subprocess.Popen(mkvmerge)
	mkvmergeProcess.communicate()
	print 'Done processing: ', episode
	i += 1

# Eject DVD
ejectProcess = subprocess.Popen(['eject', dvdrom])
ejectProcess.communicate()
