#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
from collections import defaultdict
import argparse, subprocess, pprint, re, locale

encoding = locale.getdefaultlocale()[1]

def print_tab():
	print('    ', end='')

parser = argparse.ArgumentParser()
parser.add_argument('--pool', action='store')
args = parser.parse_args()


zpool_bin = '/sbin/zpool'
sudo_bin = '/usr/bin/sudo'
zpool_cmd_list = [sudo_bin, zpool_bin, 'status', args.pool]
zpool_status = subprocess.Popen(zpool_cmd_list, stdout=subprocess.PIPE)

status = defaultdict(lambda: defaultdict(str))
critical_errors = []
total_errors = 0
error_flag = False

has_text = re.compile(b'\D+') # any non digit

for line in zpool_status.stdout:
	line = line.strip()
	if line.startswith(b'scsi') or line.startswith(b'ad') or line.startswith(b'da') or line.startswith(b'sd') or line.find(b'zfs') > -1 or line.find(b'luks') > -1:
		values = line.split()
		disk = values[0]
		read_err = values[2]
		
		if line.find(b'UNAVAIL') > -1:
			error_flag = True
			critical_errors.append(disk)
			continue

		# Do some string checks in case number of errors climbs to high (e.g. 2.15k)
		if re.search(has_text, read_err) is not None or  int(read_err) > 0:
			error_flag = True
			total_errors += int(read_err)
		write_err = values[3]
		if re.search(has_text, write_err) is not None or int(write_err) > 0:
			error_flag = True
			total_errors += int(write_err)
		chksum_err = values[4]
		if re.search(has_text, chksum_err) is not None or int(chksum_err) > 0:
			error_flag = True
			total_errors += int(chksum_err)
		status[disk]['read'] = read_err
		status[disk]['write'] = write_err
		status[disk]['chksum'] = chksum_err

if error_flag:
	print('Errors exist for pool: ' + args.pool)

if len(critical_errors) > 0:
	print('Critical Errors')
	print('---------------')
	for disk in critical_errors:
		print(disk)
	print()

print('Details')
print('-------')
for key in status.keys():
	print(key.decode(encoding))
	for subkey in status[key].keys():
		print_tab()
		print(subkey, end='')
		print(': ', end='')
		print(status[key][subkey].decode(encoding))

