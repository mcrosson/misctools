#!/bin/sh

# Get a sane input separator that will work to chop each line
IFS='
'

# Process each line of stats into array of status attribut and value
for LINE in `/usr/local/sbin/apcaccess`; do
  echo $LINE
  NAME=`echo $LINE | cut -d: -f 1  | sed -e 's/^ *//g' -e 's/ *$//g'`
  VALUE=`echo $LINE | cut -d: -f 2- | sed -e 's/^ *//g' -e 's/ *$//g'`
  echo "-${NAME}, ${VALUE}-"
  echo "====================="
done

