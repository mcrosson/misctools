#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import os, sys, ftplib, argparse, subprocess
import progressbar

pbar = None

def handlestatus(block):
    pbar.update(pbar.currval+len(block))

def ftp_upload(username, password, host, port, filename, source, directory=None):
    ftp = ftplib.FTP()
    ftp.connect(host, port)
    ftp.login(username, password)
    ftp.sendcmd("TYPE i") # Ensure we end up in binary only mode
    ftp.set_pasv(True)
    if directory is not None:
        try: 
            ftp.cwd(directory)
        except Exception:
            ftp.mkd(directory)
        finally:
            ftp.cwd(directory)
    try:
        ftp.storbinary("STOR " + filename, open(source, 'rb'),
                       callback = handlestatus, blocksize = 1024)
    except (Exception), exc:
        print(exc)

def copy_with_prog(src_file, dest_file, block_size = 1024):
    
    # Open src and dest files, get src file size
    src = open(src_file, "rb")
    dest = open(dest_file, "wb")

    src_size = os.stat(src_file).st_size
    
    # Start copying file
    while True:
        cur_block = src.read(block_size)
        
        # Update progress bar
        handlestatus(cur_block)
        
        # If it's the end of file
        if not cur_block:
            # ..write new line to prevent messing up terminal
            sys.stderr.write('\n')
            break
        else:
            # ..if not, write the block and continue
            dest.write(cur_block)
    #end while

    # Close files
    src.close()
    dest.close()

    # Check output file is same size as input one!
    dest_size = os.stat(dest_file).st_size

    if dest_size != src_size:
        raise IOError(
            "New file-size does not match original (src: %s, dest: %s)" % (
            src_size, dest_size)
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='copy a file with progress bar')
    parser.add_argument('--ftp', action='store_true', 
                        help='copy the file using ftp')
    parser.add_argument('--box', action='store_true',
                        help='mount box.net mountpoint')
    parser.add_argument('--mountpoint', action='store',
                        help='box.net mountpoint')
    parser.add_argument('--file', action='store',
                        help='the source file to copy')
    parser.add_argument('--destination', action='store',
                        help='the directory to copy to')
    parser.add_argument('--user', action='store',
                        help='the ftp username')
    parser.add_argument('--password', action='store',
                        help='the ftp password')
    parser.add_argument('--host', action='store',
                        help='the ftp host')
    parser.add_argument('--port', action='store',
                        help='the ftp port')

    args = parser.parse_args()

    widgets = ['Transferring: ', progressbar.Percentage(), ' ', progressbar.Bar(marker=progressbar.RotatingMarker()),
           ' ', progressbar.ETA(), ' ', progressbar.FileTransferSpeed()]

    if os.path.getsize(args.file) == 0:
        print("Skipping %s (size 0 file)" % args.file)
    else:
        print("Transferring %s " % args.file)
        pbar=progressbar.ProgressBar(widgets=widgets, maxval=os.path.getsize(args.file)).start()

        if args.ftp:
            ftp_upload(username=args.user, password=args.password, host=args.host, port=args.port,
                        source=args.file, filename=os.path.basename(args.file), directory=args.destination)
        else:
            if args.box:
                subprocess.check_call(['sudo', 'mount', '-o', 'uid=1000', '-t', 'davfs', 'https://www.box.com/dav', args.mountpoint])
            if args.destination is not None:
                try: 
                    os.makedirs(args.destination)
                except Exception:
                    print('Error making dav folder -- likely harmless')
            copy_with_prog(src_file=args.file, dest_file=os.path.join(args.destination, os.path.basename(args.file)))
            if args.box:
                subprocess.check_call(['sudo', 'umount', args.mountpoint])

        pbar.finish()
