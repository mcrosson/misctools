#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import subprocess

sync = ['/usr/bin/emerge', '--sync']
sync_cmd = subprocess.Popen(sync, stdout=subprocess.PIPE)
for line in sync_cmd.stdout:
	output = 0 # Do nothing so pipe doesnt get full and block for read

emerge_cmd = ['/usr/bin/emerge',
		'--deep',
		'-u',
		'-v',
		'-p',
		'world']
emerge = subprocess.Popen(emerge_cmd, stdout=subprocess.PIPE)

output = ''
for line in emerge.stdout:
	output = output + line.strip() + '\n'

zsend = ['/usr/bin/zabbix_sender',
		'-z',
		'malaria',
		'-s',
		'malaria',
		'-k',
		'emerge_updates',
		'-o',
		output]

subprocess.check_call(zsend, stdout=subprocess.PIPE)
