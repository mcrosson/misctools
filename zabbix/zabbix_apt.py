#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Works on an alternative apt-cache outside the standard system
# This allows run as non-root users
# Copy /etc/apt -> /opt/zabbix/apt/etc
# Copy /var/cache -> /opt/zabbix/apt/var/cache
#     Remove archives folder
# Copy /var/lib/apt -> /opt/zabbix/apt/var/lib/apt
# Copy /var/lib/dpkg -> /opt/zabbix/apt/var/lib/dpkg

from __future__ import print_function
import apt, subprocess

apt_cache_dir = None #'/opt/zabbix/apt'

# Sane formatting of human readable byte strings
def sizeof_fmt(num):
    for x in ['bytes','KB','MB','GB']:
        if abs(num) < 1024.0:
            return "%3.1f%s" % (num, x)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')

# Print a tab w/o a new line @ end of line
def print_tab():
	print('    ', end='')

# Print a packages detailed information
def package_as_str(pkg):
	to_return = pkg.name + '\n'
	if pkg.installed is not None:
		to_return = to_return + '    ' + pkg.installed.version
		to_return = to_return + ' -> '
	to_return = '\n'.join([
		to_return,
		pkg.candidate.version,
		'    Download size: ' + str(sizeof_fmt(pkg.candidate.size)),
		'    ' + pkg.candidate.description])
	return to_return


# Update the cache and setup things as appropriate
cache = apt.Cache(rootdir=apt_cache_dir) # Open cache
cache.update() # Update cache
cache.open(None) # Re-read the updated package list

# Get list of packages for update and various bits of info needed for output
#    With and w/o dist-upgrade set
cache.upgrade() # Get non-held updates
non_held_updates = set(cache.get_changes())
non_held_size = cache.required_space
cache.clear() # Reset the cache
cache.upgrade(True) # Run the dist-upgrade version for changes
all_updates = set(cache.get_changes())
all_size = cache.required_space
held_size = all_size - non_held_size
cache.clear() # Reset the cache (we should be done with it now)
held_updates = all_updates - non_held_updates # Get a list of the held updates

# Print information about updates
output = 'Summary'
output = '\n'.join([output,'-------'])

if len(all_updates) < 1:
	output = '\n'.join([output,'No packages to update',''])
else:
	output = '\n'.join([
		output,
		'Number of outdated packages: ' + str(len(all_updates)),
		'    Number non-held: ' + str(len(non_held_updates)),
		'    Number held: ' + str(len(held_updates)),
		''])

	if all_size < 0:
		output = output + 'Amount of space freed after all updates: '
	else:
		output = output + 'Amount of space used after all updates: '
	output = output + str(sizeof_fmt(abs(all_size))) + '\n'

	if non_held_size < 0:
		output = output + '    Amount of space freed after non-held updates: '
	else:
		output = output + '    Amount of space used after non-held updates: '

	output = output + str(sizeof_fmt(abs(non_held_size))) + '\n'

	if held_size < 0:
		output = output + '    Amount of space freed after held updates: '
	else:
		output = output + '    Amount of space used after held updates: '

	output = output + str(sizeof_fmt(abs(held_size))) + '\n'

	total_download_size = 0
	non_held_download_size = 0
	held_download_size = 0
	for pkg in all_updates:
		total_download_size += pkg.candidate.size
		if pkg in non_held_updates:
			non_held_download_size += pkg.candidate.size
		else:
			held_download_size += pkg.candidate.size

	output = '\n'.join([
				output,
				'Total download size: ' + str(sizeof_fmt(total_download_size)),
				'    Size of non-held: ' + str(sizeof_fmt(non_held_download_size)),
				'    Size of held: ' + str(sizeof_fmt(held_download_size)),
				'',
				''])


	output = output + 'Non-held Packages' + '\n'
	output = output + '--------' + '\n'
	# Print a lit of changes
	for pkg in non_held_updates:
		output = output + package_as_str(pkg) + '\n'

	output = output + 'Held Packages' + '\n'
	output = output + '--------' + '\n'
	# Print a lit of changes
	for pkg in held_updates:
		output = output + package_as_str(pkg) + '\n'

zsend = ['/usr/bin/zabbix_sender',
		'-z',
		'localhost',
		'-s',
		'influenza',
		'-k',
		'apt_updates',
		'-o',
		output]
subprocess.check_call(zsend, stdout=subprocess.PIPE)

