#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import subprocess

ps_bin = '/bin/ps'
ps_cmd = [ps_bin, '-C', 'upsmon']

ps = subprocess.Popen(ps_cmd, stdout=subprocess.PIPE)

count = 0
for line in ps.stdout:
	count += 1

print(count - 1)

