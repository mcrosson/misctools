#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Printer levels: A program to find all HP printer cartridge levels
for printers listed in a json config file.  This will write out a log
so the levels are not polled more than once per hour.

Purposful misfeature: this will poll each configured printer when the
cache has expired."""

"""
Copyright (c) 2012, Nusku Networks
Contributor: Michael Crosson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Nusku Networks nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from collections import defaultdict
import json, subprocess, pprint, argparse, locale

encoding = locale.getdefaultlocale()[1]

json_example="""
{
	"HP LaserJet 1320": 
	{
		"Device URI": "hp:/net/hp_LaserJet_1320_series?ip=10.5.5.6",
		"Toners": {
			"Black Toner Cartridge": "Q5949A/Q5949X",
			"":""
		}
	},
	"HP Color LaserJet CP1025nw": 
	{
		"Device URI": "hp:/net/HP_LaserJet_CP1025nw?ip=10.5.5.7",
		"Toners": {
			"Black Toner Cartridge": "CE310A",
			"Cyan Toner Cartridge": "CE311A",
			"Magenta Toner Cartridge": "CE313A",
			"Yellow Toner Cartridge": "CE312A"
		}
	}
}
"""

parser = argparse.ArgumentParser(description='Find printer toner levels')
parser.add_argument('--printer', action='store', required=True)
parser.add_argument('--toner', action='store', required=True, choices=['black','cyan','magenta','yellow'])
args = parser.parse_known_args()

printers = None
with open('/home/mcrosson/opt/bin/zabbix/printers.json', 'r') as printers_f:
	printers = json.loads(printers_f.read())

toners = defaultdict(lambda: defaultdict(int))

for printer in printers.keys():
	cmd_list = ['/usr/bin/hp-info', '-x', '-d', printers[printer]['Device URI']]
	cmd = subprocess.Popen(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	(cmd_output, cmd_error) = cmd.communicate()
	sku = ''
	level = ''
	for line in cmd_output.split(b'\n'):
		stripped_line = line.strip() # Removes leading and trailing whitespace
		if len(stripped_line) > 3:
			#print(stripped_line)
			split_line = stripped_line.split(b'-', 1) # Splits the line on the first '-' character
			#print(split_line)
			if len(split_line) == 2:
				agent = split_line[0]
				data = split_line[1].split()
				#print(agent)
				#print(data)
				if len(data) == 2:
					#toners[printer][agent][data[0]] = data[1]
					if data[0].find(b'sku') > -1:
						sku = data[1].decode(encoding)
					elif data[0].find(b'level') > -1 and data[0].find(b'trigger') < 0:
						level = data[1].decode(encoding)
				
				if sku is not None and level is not None:
					#print(printer)
					#print(sku)
					#print('--------')
					toners[printer][sku] = level
					sku = None
					level = None

levels = defaultdict(lambda: defaultdict(int))

for printer in printers.keys():
	for toner in printers[printer]['Toners'].keys():
		if toner != '':
			sku = printers[printer]['Toners'][toner]
			levels[printer][toner] = toners[printer][sku]

toner_key = ''
zab_key = ''
if args[0].toner == 'black':
	toner_key = 'Black Toner Cartridge'
	zab_key='black_toner_level'
elif args[0].toner == 'cyan':
	toner_key = 'Cyan Toner Cartridge'
	zab_key='cyan_toner_level'
elif args[0].toner == 'magenta':
	toner_key = 'Magenta Toner Cartridge'
	zab_key='magenta_toner_level'
elif args[0].toner == 'yellow':
	toner_key = 'Yellow Toner Cartridge'
	zab_key='yellow_toner_level'

zab_host = 'colorlaser'
if args[0].printer.find('1320') >= 0:
	zab_host = 'laserjet'

#print(args[0].printer)
#print(levels[args[0].printer])
#print('-------')
#print(levels[args[0].printer][toner_key])

zsend = ['/usr/bin/zabbix_sender',
	'-z',
	'127.0.0.1',
	'-s',
	zab_host,
	'-k',
	zab_key,
	'-o',
	str(levels[args[0].printer][toner_key])
]
subprocess.check_call(zsend, stdout=subprocess.PIPE)

#-- write out cache file here if it was out of date
#-- timestamp collected @ start of script (see datetime)

