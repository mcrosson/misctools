#!/usr/bin/env bash
# Disable elasticsearch transaction auto-flushing
curl -XPUT 'malaria:9200/_settings' -d '{
	"index" : {
		"translog.disable_flush" : "true"
	}
}' >> /dev/null 2>&1 # Don't care if the result is ok or not ok, deal with ramifications later

curl -XPOST 'malaria:9200/_flush' >> /dev/null 2>&1 # Flush so things don't disappear on rsync; this is needed because turning off flush doesn't cause a flush op

sleep 10

# Run backup 
/home/mcrosson/opt/bin/zrb --ssh --destination tank/Servers/malaria/gentoo/hardened --source mcrosson@malaria:/ --sudo-rsync --exclude=/dev,/sys,/proc,/run,/tmp,/mnt/malaria-btrfs,/scratch,/opt/splunk/var --zabbix-send --zabbix-server malaria --zabbix-hostname influenza --zabbix-key backup_malaria_root

# Flush the transactions
curl -XPOST 'malaria:9200/_flush' >> /dev/null 2>&1 # Don't care if the result is ok or not ok, deal with ramifications later

# Turn on auto-flushing
curl -XPUT 'malaria:9200/_settings' -d '{
	"index" : {
		"translog.disable_flush" : "false"
	}
}' >> /dev/null 2>&1 # Don't care if the result is ok or not ok, deal with ramifications later

# Flush once more for good measure
curl -XPOST 'malaria:9200/_flush' >> /dev/null 2>&1 # Don't care if the result is ok or not ok, deal with ramifications later

