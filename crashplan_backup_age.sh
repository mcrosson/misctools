#!/bin/bash

#	Check Crashplan Currency - GNU
#	by Jedda Wignall
#	http://jedda.me

#	v1.0.1 - 3 May 2012
#	Added comments and fixed broken bits.

#	v1.0 - 27 Apr 2012
#	Initial release.

#	This script checks the currency of a CrashPlan backup on Linux. There is a different version for Mac OS X due to differences between date on GNU and BSD.
#	Takes three arguments ([-d] cp.properties file in backup destination, [-w] warning threshold in minutes, [-c] critical threshold in minutes):
#	./check_crashplan_currency_gnu.sh -d /media/Backups/52352423423424243/cp.properties -w 240 -c 1440

currentDate=`date "+%s"`
cpDirectory=""
warnMinutes=""
critMinutes=""

while getopts "d:c:" optionName; do
case "$optionName" in
d) cpDirectory=("$OPTARG");;
c) cpCompletedBackup={true};;
esac
done


# check to see if the cp.properties file exists
if ! [ -f "$cpDirectory" ];
then
	echo "CRITICAL ERROR - the CrashPlan backup you pointed to does not exist!"
	exit 2
fi

if [ $cpCompletedBackup ];
then
	lastBackupLine=`grep -n lastCompletedBackupTimestamp "$cpDirectory"`
else
	lastBackupLine=`grep -n lastBackupTimestamp "${cpDirectory}"`
fi
if [ -z "$lastBackupLine" ]; then
	echo "-1"
	#exit 2
	exit 0
fi
if [ $cpCompletedBackup ];
then
	lastBackupDateString=`echo $lastBackupLine | awk -F lastCompletedBackupTimestamp= '{print $NF}' | sed 's/.\{5\}$//' | sed 's/\\\//g'`
else
	lastBackupDateString=`echo $lastBackupLine | awk -F lastBackupTimestamp= '{print $NF}' | sed 's/.\{5\}$//' | sed 's/\\\//g'`
fi
lastBackupDate=$(date -d "$lastBackupDateString" "+%s" )
diff=$(( $currentDate - $lastBackupDate)) # Units: Seconds
echo $diff
exit 0
